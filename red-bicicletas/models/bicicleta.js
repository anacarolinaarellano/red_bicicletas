var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var biciclietaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: {type: '2dsphere', sparse: true}
    }
});

biciclietaSchema.statics.createInstance = function(code, color, modelo, ubicacion){
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};

biciclietaSchema.methods.toString = function() {
    return 'code: ' + this.code + ' | color: ' + this.color;
};

biciclietaSchema.statics.allBicis = function(cb){
    return this.find({}, cb);
};

biciclietaSchema.statics.add = function(aBici, cb){
    this.create(aBici, cb);
},

biciclietaSchema.statics.findByCode = function(aCode, cb){
    return this.findOne({code: aCode}, cb);
};

biciclietaSchema.statics.removeByCode = function(aCode, cb){
    return this.deleteOne({code: aCode}, cb);
};
module.exports = mongoose.model('Bicicleta', biciclietaSchema);


