#Información general
Este repositorio contiene la primara tarea del curso "Desarrollo dle lado del lado del servido: NodeJS, Express y MongoDB" referente al proyecto red-bicicletas.

##Tecnologías
NodeJS
Express

##Setup
Para correr este proyecto, instálalo localmente, abre la carpeta red-bicicletas y corre los comandos:

```
$npm install
$npm run devstart
```
 
##Información adicional
Para ver el listado de bicicletas ir a la dirección http://localhost:3000/bicicletas
Para agregar bicicletas ir a la dirección http://localhost:3000/bicicletas/create
Para editar y eliminar bicicletas presionar los botones correspondientes dentro del listado de bicicletas.

###Para probar la API abrir la aplicación Postman
Para ver el listado de bicicletas seleccionar GET, ingresar la dirección http://localhost:3000/api/bicicletas y presionar "Send".
Para crear una bicicleta seleccionar POST, ingresar la dirección http://localhost:3000/api/bicicletas/create, ingresar en body todos los datos de la nueva bicicleta y presionar "Send".
Para eliminar una bicicleta seleccionar DELETE, ingresar la dirección http://localhost:3000/api/bicicletas/delete, ingresar en body un id de bicicleta y presionar "Send".
Para actualizar una bicicleta seleccionar POST, ingresar la dirección http://localhost:3000/api/bicicletas/update, ingresar en body los datos de la bicicleta y presionar "Send".